void main() {
    String firstName = 'John';
    Building building = new Building(
      name : 'Caswynn',
      address :  '134 Timog ave. brgy. sacred heart, Quezon City, Metro Manila',
      floors : 8
      
       
       );
    building.name = 'Caswynn';
    building.floors = 8;
    building.address = '134 Timog ave. brgy. sacred heart, Quezon City, Metro Manila';

    

    print(building);
    print(building.name);
    print(building.floors);
    print(building.address);
    
}


class Building {
    String? name;
    int floors;
    String address;
    

    Building({//required is needed for optional
        this.name,
        required this.floors,
        required this.address
        }) {
    print ('a building object has been created');
  }
}


//After adressing the nullable fields
/*
class Building {
    String name;
    int floors;
    String address;
    List<String> rooms;

    Building(this.name, this.floors, this.address, this.rooms){
    print ('a building object has been created');
}
}

*/
//before addressing the nullable fields
/*
class Building {//late value of name will be given at a later execution of code
    late String name;
    late int floors;
    late String address;

    Building(String name, int floors, String address) {
      this.name = name;
      this.floors = floors;
      this.address = address;
    }
}
*/
//the class declaration looks like this
/*
class ClassName
<field>
<constructors>
<methods>
<getters-or-setters>
*/