// to know when top use late vs required 
//use 'required' when logically you always require that variable to have a value

//use 'late' when a variable can have a null value
//use 'late' if yoiu are sure that later on your program that value will be given to that value
//Else, use null-enabler instead(?)
class Building {
    String? _name;
    int floors;
    String address;
    
//required is needed for optional
    Building(
      this._name,

      {

        required this.floors,
        required this.address
        }) {
    print ('a building object has been created');
  }
  //the get and set allows indirect access to class field
  //setter
  void set Name(String? name ){
    this._name = name;
print('The $name building name has been changed');
this._name = name;
  }
//getter
  String? get Name {
    
    print('the building is retrieved');
    return this._name;
  }


  Map <String, dynamic> getProperties(){
    return{
      'name' : this._name,
      'floors' : this.floors,
      'address' : this.address
    };
  }
}