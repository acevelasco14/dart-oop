void main() {
  //List<dataType> variableName = [value]
  List<int> discountRanges = [20, 40, 60, 80];
  List<String> names = [ //const makes list unmodifiable list
    'John',
    'Jane',
    'Tom'
  ];
  const List<String> maritalStatus = [
    'single',
    'married',
    'divorced',
    'widowed'

  ];
  print(discountRanges);
  print(names);
  print(discountRanges[2]);
  print(names[0]);
  print(discountRanges.length);
  print(names.length);//length of array

  names[0] = 'Jonathan';
  
  print(names);

  names.add('Mark');//add() add element in end of the  array
  names.insert(0,'Roselle');//insert() add element beginning of the  array
  print(names);

  print(names.isEmpty);//to check if there are elements inside a list
  print(names.isNotEmpty);//to check if there are no elements inside a list
  print(names.first);
  print(names.last);
  print(names.reversed);

  //this is a method
  //this method modifies the list itself
  names.sort();
  print(names.reversed);
  print(names);
}