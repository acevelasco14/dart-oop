void main() {
  //String firstName = 'John';//preferred format
  //Set<dataType variableName = {values};

  // a set in dart is an unordered collection of unit items
  Set<String> subcontractors = {'Sonderhoff', 'Stahlschmidt'};
  subcontractors.add('Schweisstechnik');
  subcontractors.add('Kreatel');
  subcontractors.add('Kunstoffe');
  subcontractors.remove('Sonderhoff',);
  print(subcontractors);
}