import './worker.dart';

void main() {
   // Person personA = new Person();
   Doctor doctor = new Doctor(firstName: 'John', lastName: 'Smith');
   Carpenter carpenter = new Carpenter();

   print(doctor.getFullName());
}



abstract class Person {
    //the class Person defines that a 'getFullName' must be implemented.
    //However, it does not tell the concrete class how to implement it

    String getFullName();
}
// the concrete class is the doctor class
class Doctor implements Person{
    String firstName;
    String lastName;


    Doctor({
        required this.firstName,
        required this.lastName
    });

    String getFullName(){
        return 'Dr. ${this.firstName} ${this.lastName} ';
    }
}

class Attornet implements Person {
    String getFullName(){
        return 'Atty. ';
    }
}