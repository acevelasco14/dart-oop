abstract class _Worker{
    String getType(); 
       
    
}

class Carpenter implements _Worker{
    //@override is an annotation
    String getType(){
        return 'Carpenter';
    }
}

