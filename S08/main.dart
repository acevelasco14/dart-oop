void main() { 
    Bulldozer bulldozer = new Bulldozer(
       equipmentType: 'bulldozer', 
       equipmentModel: 'Caterpillar D10', 
       bladeType: 'U blade'
       );
    Towercrane towerCrane = new Towercrane(
        equipmentType: 'tower crane', 
        equipmentModel: '370 EC-B 12 Fibre', 
        hookRadius: '78m', 
        hookCapacity: '12000');
    Loader loader = new Loader(equipmentType: 'loader', 
        equipmentModel: 'Volvo L60H', 
        loaderType: 'wheel loader', 
        maxCapacity: '16530 lbs');

    List<Equipment> equipment = [];
        equipment.add(bulldozer);
        equipment.add(towerCrane);
        equipment.add(loader);
    
    for (var i = 0; i < equipment.length; i++) {
        print(equipment[i].describe());
    }
}

abstract class Equipment {
    String describe();
}

class Bulldozer implements Equipment {
    String equipmentType;
    String equipmentModel;
    String bladeType;


    Bulldozer({
        required this.equipmentType,
        required this.equipmentModel,
        required this.bladeType
    });

    String describe(){
        return 'The ${this.equipmentType} ${this.equipmentModel} has a ${bladeType}.';
    }
}

class Towercrane implements Equipment {
    String equipmentType;
    String equipmentModel;
    String hookRadius;
    String hookCapacity;

    Towercrane({
        required this.equipmentType,
        required this.equipmentModel,
        required this.hookRadius,
        required this.hookCapacity

    });

    String describe(){
        return 'The ${this.equipmentType} ${this.equipmentModel} has a radius of ${this.hookRadius} hook radius and a max capacity ${this.hookCapacity}.';
    }
}

class Loader implements Equipment {
    String equipmentType;
    String equipmentModel;
    String loaderType;
    String maxCapacity;
  
    Loader({
        required this.equipmentType,
        required this.equipmentModel,
        required this.loaderType,
        required this.maxCapacity

    });

    String describe(){
        return 'The ${this.equipmentType} ${this.equipmentModel} is a ${this.loaderType} and has a tipping load of ${this.maxCapacity}.';
    }
}


