//the keyword 'void' means the function will not return anything
//the syntax of a function is: return-type function-name(parameter){ }
void main() {
    String firstName = 'John';
    String? middleName = null;
    String lastName = 'Smith';
    int age = 31; //for whole numbers without decimal points
    double height = 172.45; //for numbers with decimal points
    num weight = 64.32; //can accept numbers with or without decimal points
    bool isRegistered = false;
    List<num> grades = [98.2, 89, 87.88, 91.2];//List is an array
    //Map person = new Map(); //Map is an object, one method is to create new Map 
    Map<String, dynamic> personA = { //Map is an object
     'name' : 'Brandon',
     'batch' : 213
    };


    //Alternative syntax for declaring an object
    Map<String, dynamic> personB = new Map();
    personB['name'] = 'Juan';
    personB['batch'] = 89;
   
   //with final, once value has been set, it cannot be changed.
    final DateTime now;
    now = DateTime.now();

    //with const, an idetifier must have a correspondiong declaration of value
    const String companyAcronym = 'FFUF';
    //companyAcronym = 'FFUF';




/*    Map<num, dynamic> person = { //Map is an object
     1 : 'Brandon',
     2 : 213
    };*/

//    var number = 1;
//    name = number.toString();

//    print(firstName +' '+ lastName); //the 'Hello' is concatinated with the name variable
    print('Full Name: $firstName $lastName');
    print('Age: ' +  age.toString());
    print('height: ' + height.toString());
    print('weight: ' + weight.toString());
    print('Registered: ' + isRegistered.toString());
    print('Grades: ' + grades.toString());
    print('Current DateTime' + now.toString());


    print(personB['name']);
    //TypeScript -> JavaScript
    //Dart ------> TypeScript -> JavaScript
}