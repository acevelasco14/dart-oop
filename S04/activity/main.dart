void main() {
    Function discountBy20 = getDiscount(20);
    Function discountBy40 = getDiscount(40);
    Function discountBy60 = getDiscount(60);
    Function discountBy80 = getDiscount(80);


  num total = 0;


  const numbers = [45, 34.2, 176.9, 32.2];
  for (var i = 0; i < numbers.length; i++) {
    total += numbers[i];
  }
  print("${total} (no discount)");
  print("${discountBy80(total)} (20% discount) ");
  print("${discountBy60(total)} (40% discount) ");
  print("${discountBy40(total)} (60% discount) ");
  print("${discountBy20(total)} (80% discount) ");
}
Function getDiscount(num percentage) {
  return (num amount) { 
    return amount * percentage / 100;
  };
}