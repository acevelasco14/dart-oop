//A variable must be a noun
//a function must be a verb

void main() {
  
    List<num> prices = [45, 34.2, 176.9, 32.2];
    num totalPrice = getTotalPrice(prices);

  print(totalPrice);
  print(getDiscountedPrice(totalPrice, .2));
  print(getDiscountedPrice(totalPrice, .4));
  print(getDiscountedPrice(totalPrice, .6));
  print(getDiscountedPrice(totalPrice, .8));

}
num getTotalPrice(List<num> prices){
    num totalPrice = 0;

    prices.forEach((num price){
      totalPrice += price;
    });

  return (totalPrice);
}

//one-line alternative to get total price
//num getTotalPrice(List<num> prices) => prices.reduce((numA, numB) => numA + numB);

num getDiscountedPrice(num totalPrice, num percentage){
  return totalPrice - (totalPrice * percentage);
}


/*void main() {
    Function discountBy20 = getDiscount(20);
    Function discountBy40 = getDiscount(40);
    Function discountBy60 = getDiscount(60);
    Function discountBy80 = getDiscount(80);
    List<num> numList = [45, 34.2, 176.9, 32.2];

  print(getTotal(numList));
  print("${discountBy80(getTotal(numList))} (20% discount) ");
  print("${discountBy60(getTotal(numList))} (40% discount) ");
  print("${discountBy40(getTotal(numList))} (60% discount) ");
  print("${discountBy20(getTotal(numList))} (80% discount) ");
  
}
num getTotal(numList, {total = 0 }){
  numList.forEach((i) => total += i);
  return total;
}

Function getDiscount(num percentage) {
  return (num amount) { 
    return amount * percentage / 100;
  };
}
*/
