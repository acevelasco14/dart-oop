
//The main() function is the entry point of dart progrram
void main(){
  String companyName = getCompanyName();
  print(getCompanyName());
  print(getYearEstablishment());
  //print(hasOnlineClasses());
  //print(getCoordinates());
  //print(combineAddress('134 Timog Avenue', 'Barangay Sacred Heart', 'Quezon City', 'Metrro Manila'));
  //print(combineName('John', 'Smith'));
  //print(combineName('John', 'Smith', isLastNameFirst: true));
  //print(combineName('John', 'Smith', isLastNameFirst: false));

  
  List<String> persons = ['John Doe', 'Jane Doe'];
  List<String> students = ['Nicolas Rush', 'James Holden'];
//anonymous function
 /* persons.forEach((String person) { 
    print(person);
  });

  students.forEach((String person) { 
    print(person);
  });
}*/
print(isUnderAge(18));


//functions as objects And used as an argument
//printName(value) is functionm execution/call/invocation
//printName is reference to the given function
  persons.forEach(printName);

  students.forEach(printName);
  }
 
void printName(String name) {
  print(name);
}




//Optional named parameters
//these are parameters added after the required ones.
//these parameters are added inside a curly bracket
//if no value is given , a default value can be assigned
String combineName(String firstName, String lastName, {bool isLastNameFirst = false}) {
  if(isLastNameFirst){
    return '$lastName, $firstName';
  }else{
    return '$firstName $lastName';
  }
  
  //John undefined
}

String combineAddress(String specifics, String barangay, String city, String province){
  //return specifics + ', ' + barangay +', ' + city + ', ' + province;
  return '$specifics, $barangay, $city, $province' ;
}

String getCompanyName() {
  return 'FFUF';
}

int getYearEstablishment() {
  return 2021;
}

bool hasOnlineClasses() {
  return true;
}
/*
bool isUnderAge(int age) {
  return (age < 18)? true : false;
}
//the initial isUnderAge function can be changed to a lambda or arrow function
*/
//the lambda functiioon is a shortcut functioin for returning values from simple operations
//the syntax of a lambda function is return-type function-name(parameters) => expression;
bool isUnderAge(int age) => age < 18? true : false;


Map<String, double> getCoordinates() {
  return {
    'latitude': 14.634702,
    'longtitude': 121.043716
  };
}

//the following syntax is followed when creating a function
/*
return-type funtion-name(param-data-type parameterA,param-data-type parameterB) {
  //code login inside the function
  return 'return-value'
}
*/

//Arguments are the values being passed to the function
//parameter are the values being received by the function

//x (parameter) = 5 (argument)


// if no date type is specified to a paramaeter, its default data type is dynamic