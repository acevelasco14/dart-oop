void main() {
  Function discountBy25 = getDiscount(25);
  Function discountBy50 = getDiscount(50);
//the discountBy25 is then considered as closure
//the closure has access to variables in its lexical scope
  print(discountBy25(1400));
  print(discountBy50(1400));

  print(getDiscount(25)(1400));
  print(getDiscount(50)(1400));
}

Function getDiscount(num percentage) {
  //when the getDiscount is used and the function below is returned
  //the value of 'percentage' paramater is retained
  //that is called lexecal scope
  return (num amount) { 
    return amount * percentage / 100;
  };
}