void main() {
  //assignment operator
  int x = 1397;
  int y = 7831;

  //Arithmetic operators

  num sum = x + y;
  num difference = x-y;
  num product = x * y;
  num quotient = x / y;
  num remaider = x % y;
  num output = (x * y) - (x / y + x);

  print(remaider);
  print(output);

  bool isGreaterThan = x > y;
  bool isLessThan = x<y;

  bool isGTorEqual = x >= y;
  bool isLTorEqual = x <= y;

  bool isEqual = x == y;
  bool isNotEqual = x != y;

  //logical operators
  bool isLegalAge = true;
  bool isRegistered = false;

  bool areAllRequirementsMet = isLegalAge && isRegistered;
  bool areSomeRequirementsMet = isLegalAge || isRegistered;
  bool isNotRegistered = !isRegistered;

  //Increment & Decrement operators 
  print(x++);
  print(y--);

}