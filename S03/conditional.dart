void main() {
  print(determineTyphoonIntensity(67));
  print(showServiceItems('robotics'));

}

List<String> showServiceItems(String category) {
  if (category == 'apps'){
    return ['native', 'android', 'ios', 'web'];
  }else if (category == 'cloud' ) {
    return ['azure', 'microservices'];
  }else if (category == 'robotics' ) {
    return ['sensors', 'fleet-tracking', 'realtime-communication'];
  
  }else{
    return [];
  }
}

String determineTyphoonIntensity(int windSpeed){
  if (windSpeed < 30){
    return 'Not a typhoon yet';
  } else if (windSpeed <= 61) {
    return 'tropical depression detected';
  }else if (windSpeed >= 62 && windSpeed <= 88) {
    return 'tropical storm detected';
  } else if (windSpeed >= 89 && windSpeed <= 117) {
    return 'severe tropical storm detected';
  }else{
    return 'typhoon detected';
  }
  
}

String selectSector(int sectorID) {
  String name;
  switch(sectorID) {
    case 1: 
      name = 'Craft';
      break;
    case 2:
      name = 'Assembly';
      break;
    case 3:
      name = 'Building Operations';
      break;
    default:
      name = sectorID.toString() + ' is out of bounds';
      // break; optional
  }
  return name;
}

bool isUnderAge(age) {
 /* if (age < 18){
    return true;
  }else{
    return false;
  }*/
  //ternary condition
 // return (age < 18 ) ? true : false
 return age < 18;
}