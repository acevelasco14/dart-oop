void main() {
    Equipment equipment = new Equipment();
    equipment.name = 'Equipment - 001';
    print(equipment.name);

    Loader loader = new Loader();
    loader.name = 'Loader-001';
    print(loader.name);
    print(loader.getCategory());

    Car car = new Car();
    car.name = 'Car-001';
    print(car.name);
    print(car.getCategory());
    car.moveForward(30);
    car.moveBackward();
    loader.moveForward(30);
    loader.moveBackward();
    print(car.acceleration);


}

class Equipment {
    String? name;
}

class Loader extends Equipment with Movement{
    String getCategory() {
        return '${this.name} is a loader';
    }
}

class Car with Movement, Engine{
    String getCategory() {
                
        return '${this.name} is a crane';
    }
}

mixin Engine{
    void start(){

    }
    void stop(){

    }
}

mixin Movement{
    num? acceleration;
    String? name;
    void moveForward(num acceleration){
        this.acceleration = acceleration;
        print('The vehicle is moving forward');
    }
    void moveBackward(){
        print('The vehicle is moving Backward');
    }
}