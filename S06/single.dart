void main() {
  Person person = new Person(firstName: 'John', lastName: 'Smith');
  Employee employee = new Employee(firstName: 'Jonas', lastName: 'Westwood',  employeeId: 'Westwood');
  print(person.getFullName());
  print(employee.getFullName());
}

class Person {
    String firstName;
    String lastName;

  Person({
      required this.firstName,
      required this.lastName
  });
   String getFullName(){
      return '${this.firstName} ${this.lastName}';
  }
}

class Employee extends Person{
  //in spirit, the firstname and lastname is inherited by employee from peroson nad the getfullname method
  //inheritance then allows us to write less code

  String employeeId;
    Employee({
        required String firstName,
        required String lastName,
        required this.employeeId
       }): super(
         firstName: firstName,
          lastName: lastName
          ); 
    }
