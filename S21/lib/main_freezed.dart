    import './freezed_models/user.dart';
    import './freezed_models/task.dart';
    import './freezed_models/note.dart';

    void main() {
        User userA = User(id: 1, email:'john@gmail.com');
        User userB = User(id: 1, email:'john@gmail.com');
        Task taskB = Task(id: 1, userId: 1, description: 'a', isDone: 2);
        Note noteB = Note(id: 1, userId: 1, title: 'title', description: 'description');

            // print(userA.hashCode);
            // print(userB.hashCode);
            // print(userA == userB);

        //demostratiuon of object immutability below
        //Immutability means changes are not allowed
        //It ensures that an object will not be changed accidentally 

        //instead of directly changiong the objects property,
        //the object itself will be change or reassigned with new values
        //to achieve this we used the object.copyWith() method

        // print(userA.email);
        // userA = userA.copyWith(email: 'john@hotmail.com');
        // print(userA.email);

        var response =  {'id': 1, 'email': 'doe@gmail.com'};

        // User userC = User(
        // id: response['id'] as int, 
        // email: response['email'] as String
        // );

        User userC = User.fromJson(response);
        print(userC);
        print(userC.toJson());
        print(taskB);
        print(taskB.toJson());
        print(noteB);
        print(noteB.toJson());
    }

  